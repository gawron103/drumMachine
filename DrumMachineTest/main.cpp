#include <QTest>

#include "tst_pad.h"
#include "tst_usersettings.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    Tst_Pad testpad;
    Tst_UserSettings userSettings;

    return QTest::qExec(&testpad) |
            QTest::qExec(&userSettings);
}
