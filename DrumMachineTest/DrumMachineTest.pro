QT += testlib
QT += gui multimedia widgets
CONFIG += qt warn_on depend_includepath testcase c++14

TEMPLATE = app

SOURCES +=  \
    main.cpp \
    tst_pad.cpp \
    pad.cpp \
    tst_usersettings.cpp \
    usersettings.cpp

HEADERS += \
    tst_pad.h \
    pad.h \
    tst_usersettings.h \
    usersettings.h
