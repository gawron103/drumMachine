#include "tst_pad.h"

Tst_Pad::Tst_Pad()
{
    testPad = new Pad;
}

Tst_Pad::~Tst_Pad()
{
    delete testPad;
}

void Tst_Pad::testShortcutSet()
{
    const QString TEST_SHORTCUT = "A";

    testPad->setPadShortcut(TEST_SHORTCUT);

    QVERIFY(testPad->getPadShortcut() == TEST_SHORTCUT);
}

void Tst_Pad::testSoundSet()
{
    const QString TEST_SOUND = "example_sound.wav";

    testPad->setPadSound(TEST_SOUND);

    QVERIFY(testPad->getPadSound() == TEST_SOUND);
}
