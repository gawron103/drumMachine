#include "usersettings.h"

#include <QApplication>

static const QString SHORTCUT_CONFIG_FILE = "/configShortcuts.ini";
static const QString SOUND_CONFIG_FILE = "/configSounds.ini";

UserSettings::UserSettings(QObject *parent) : QObject(parent)
{
    qDebug() << QApplication::applicationDirPath() + SHORTCUT_CONFIG_FILE;
    qDebug() << QApplication::applicationDirPath() + SOUND_CONFIG_FILE;

    m_soundSettings = std::make_unique<QSettings>(QApplication::applicationDirPath() + SOUND_CONFIG_FILE, QSettings::IniFormat);
    m_shortcutSettings = std::make_unique<QSettings>(QApplication::applicationDirPath() + SHORTCUT_CONFIG_FILE, QSettings::IniFormat);
}

void UserSettings::saveShortcut(const uint8_t &id, const QString &shortcut)
{
    m_shortcutSettings.get()->setValue(QString::number(id), shortcut);
}

void UserSettings::saveSound(const uint8_t &id, const QString &sound)
{
    m_soundSettings.get()->setValue(QString::number(id), sound);
}

QString UserSettings::loadShortcuts(const uint8_t &index)
{
    return m_shortcutSettings.get()->value(QString::number(index)).toString();
}

QString UserSettings::loadSounds(const uint8_t &index)
{
    return m_soundSettings.get()->value(QString::number(index)).toString();
}
