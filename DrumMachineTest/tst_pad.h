#ifndef TST_PAD_H
#define TST_PAD_H

#include <QObject>
#include <QTest>

#include "pad.h"


class Tst_Pad : public QObject
{
    Q_OBJECT

public:
    Tst_Pad();
    ~Tst_Pad();

    Pad *testPad = nullptr;

private slots:
    void testShortcutSet();
    void testSoundSet();
};

#endif // TST_PAD_H
