#include "tst_usersettings.h"

Tst_UserSettings::Tst_UserSettings()
{
    userSettings = new UserSettings;
}

Tst_UserSettings::~Tst_UserSettings()
{
    delete userSettings;
}

void Tst_UserSettings::testSaveShortcut()
{
    const uint8_t TEST_INDEX = 33;
    const QString TEST_SHORTCUT = "A";

    userSettings->saveShortcut(TEST_INDEX, TEST_SHORTCUT);

    QVERIFY(userSettings->loadShortcuts(TEST_INDEX) == TEST_SHORTCUT);
}

void Tst_UserSettings::testSaveSound()
{
    const uint8_t TEST_INDEX = 22;
    const QString TEST_SOUND = "example_sound.wav";

    userSettings->saveSound(TEST_INDEX, TEST_SOUND);

    QVERIFY(userSettings->loadSounds(TEST_INDEX) == TEST_SOUND);
}
