#ifndef TST_USERSETTINGS_H
#define TST_USERSETTINGS_H

#include <QObject>
#include <QTest>

#include "usersettings.h"

class Tst_UserSettings : public QObject
{
    Q_OBJECT
public:
    Tst_UserSettings();
    ~Tst_UserSettings();

    UserSettings *userSettings = nullptr;

private slots:
    void testSaveShortcut();
    void testSaveSound();
};

#endif // TST_USERSETTINGS_H
