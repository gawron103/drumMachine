#ifndef PADSCONFIGURATOR_H
#define PADSCONFIGURATOR_H

#include <memory>
#include <vector>

#include "padtransformer.h"
#include "pad.h"

namespace Ui {
class PadsConfigurator;
}

/**
 * @brief The PadsConfigurator class -> class responsible for creating and holding
 * instances of pads transformers.
 */
class PadsConfigurator : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief PadsConfigurator -> constructor.
     * @param padsCount -> number of pads user wants to have.
     * @param pads -> container of existing instances of pad.
     * @param parent -> default parameter of widgets parent.
     */
    explicit PadsConfigurator(const uint8_t &padsCount, std::vector<std::shared_ptr<Pad>> &pads, QWidget *parent = 0);
    ~PadsConfigurator();

    /**
     * @brief createPadTransformer -> method that creates specific amount of pad transformer instances.
     * @param number -> number of pad instances.
     */
    void createPadTransformer(const uint8_t &number);
    /**
     * @brief placePadTransformers -> method that places pad transformers in layout.
     */
    void placePadTransformers();

private:
    Ui::PadsConfigurator *ui;

    /**
     * @brief m_numberOfPads -> const variable of number of created pads.
     */
    const uint8_t m_numberOfPads;
    /**
     * @brief m_pads -> vector of existing pads.
     */
    std::vector<std::shared_ptr<Pad>> m_pads;
    /**
     * @brief padTransformers -> vector of pad transformers.
     */
    std::vector<std::shared_ptr<PadTransformer>> padTransformers;

signals:
    /**
     * @brief soundChanged -> signal for sound change.
     * @param id -> id of pad transformer.
     */
    void soundChanged(const uint8_t id);
    /**
     * @brief shortcutChanged -> signal for shortcut change.
     * @param id -> id of pad transformer.
     */
    void shortcutChanged(const uint8_t id);
};

#endif // PADSCONFIGURATOR_H
