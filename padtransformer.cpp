#include "padtransformer.h"
#include "ui_padtransformer.h"

#include <QFileDialog>
#include <QInputDialog>

static const QString M_LABEL_DEFAULT_TEXT = "Button";

PadTransformer::PadTransformer(const std::shared_ptr<Pad> &pad, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PadTransformer),
    m_pad(pad)
{
    ui->setupUi(this);

    m_id = setId();

    changeTransformerLabel();
    changeSoundLabel();

    connect(ui->pb_changeSound, &QPushButton::clicked, this, &PadTransformer::chooseNewSound);

    connect(ui->pb_changeShortcut, QPushButton::clicked, this, &PadTransformer::createNewShortcut);
}

PadTransformer::~PadTransformer()
{
    const bool SHOULD_RESET_ID_COUNTER = true;
    setId(SHOULD_RESET_ID_COUNTER);

    delete ui;
}

int PadTransformer::setId(const bool shouldReset)
{
    static int id = -1;

    if (shouldReset)
    {
        const uint8_t RESET_VALUE = -2;
        id = RESET_VALUE;
    }

    ++id;
    return id;
}

void PadTransformer::chooseNewSound()
{
    QString inputFilePath = QFileDialog::getOpenFileName(this, tr("Open"), tr(""));

    if (inputFilePath.isEmpty())
    {
        return;
    }

    QFileInfo inputFileName(inputFilePath);

    m_pad->setPadSound(inputFileName.fileName());

    changeSoundLabel();

    emit signalSoundChanged(m_id);
}

void PadTransformer::createNewShortcut()
{
    QString newShortcut = QInputDialog::getText(this, tr("New shortcut"),
                                                tr("Input shortcut"));

    if (newShortcut.isEmpty())
    {
        return;
    }

    QString shortcut = static_cast<QString>(newShortcut.left(1));

    m_pad->setPadShortcut(shortcut);

    changeTransformerLabel();

    emit signalShortcutChanged(m_id);
}

void PadTransformer::changeTransformerLabel()
{
    ui->groupBox->setTitle(M_LABEL_DEFAULT_TEXT + " " + m_pad.get()->getPadShortcut());
}

void PadTransformer::changeSoundLabel()
{
    ui->l_soundName->setText(m_pad.get()->getPadSound());
}
