#include "padswindow.h"
#include "ui_padswindow.h"

#include <cmath>

static const QString SHORTCUT_CONFIG_FILE_NAME = "configShortcut.ini";
static const QString SOUND_CONFIG_FILE_NAME = "configSound.ini";

PadsWindow::PadsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PadsWindow)
{
    ui->setupUi(this);

    userSettings = std::make_unique<UserSettings>();

    connect(ui->actionExit, &QAction::triggered, this, &PadsWindow::close);
    connect(ui->actionChoose_pad_amount, &QAction::triggered, this, &PadsWindow::resetPadsWindow);
    connect(ui->actionConfigure_pads, &QAction::triggered, this, [this]()
    {
        m_padsConfigurator = std::make_unique<PadsConfigurator>(m_pads.size(), m_pads);

        for (uint8_t i = 0; i < m_padsCounter; i++)
        {
            m_padsConfigurator.get()->createPadTransformer(i);
        }

        m_padsConfigurator.get()->placePadTransformers();
        m_padsConfigurator->show();

        connect(m_padsConfigurator.get(), &PadsConfigurator::shortcutChanged, this, PadsWindow::saveShortcutSetting);
        connect(m_padsConfigurator.get(), &PadsConfigurator::soundChanged, this, PadsWindow::saveSoundSetting);
    });
}

PadsWindow::~PadsWindow()
{
    delete ui;
}

void PadsWindow::createPads(const uint8_t &numberOfPads)
{   
    m_padsCounter = numberOfPads;

    for (uint8_t index = 0; index < numberOfPads; ++index)
    {
        m_pads.push_back(std::make_unique<Pad>());

        loadShortcutSettings(index);
        loadSoundSettings(index);
    }

    const uint8_t PADS_PER_LINE = sqrt(numberOfPads);

    uint8_t index = 0;

    for (uint8_t column = 0; column < PADS_PER_LINE; ++column)
    {
        for (uint8_t row = 0; row < PADS_PER_LINE; ++row)
        {
            ui->gridLayout->addWidget(m_pads[index].get(), column, row);
            index++;
        }
    }

    adjustSize();
}

void PadsWindow::saveShortcutSetting(const uint8_t &id)
{
    userSettings.get()->saveShortcut(id, m_pads[id].get()->getPadShortcut());
}

void PadsWindow::saveSoundSetting(const uint8_t &id)
{
    userSettings.get()->saveSound(id, m_pads[id].get()->getPadSound());
}

void PadsWindow::loadShortcutSettings(const uint8_t &index)
{
    m_pads[index].get()->setPadShortcut(userSettings.get()->loadShortcuts(index));
}

void PadsWindow::loadSoundSettings(const uint8_t &index)
{
    m_pads[index].get()->setPadSound(userSettings.get()->loadSounds(index));
}

void PadsWindow::resetPadsWindow()
{
    m_pads.erase(m_pads.begin(), m_pads.end());

    m_startupWidget->show();
}

void PadsWindow::showStartupWidget()
{
    m_startupWidget = std::make_unique<StartupWidget>();
    m_startupWidget->show();

    connect(m_startupWidget.get(), &StartupWidget::numberOfPads, this, &PadsWindow::createPads);
    connect(m_startupWidget.get(), &StartupWidget::closeApp, this, &PadsWindow::close);
}
