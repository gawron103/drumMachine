#ifndef USERSETTINGS_H
#define USERSETTINGS_H

#include <QSettings>

#include <memory>

/**
 * @brief The UserSettings class -> class responsible for holding user settings.
 */
class UserSettings : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief UserSettings -> constructor.
     * @param parent -> default parameter of widgets parent.
     */
    explicit UserSettings(QObject *parent = nullptr);

    /**
     * @brief saveShortcut -> method that saves shortcuts choosen by user to file.
     * @param id -> pad id.
     * @param shortcut -> pad shortcut.
     */
    void saveShortcut(const uint8_t &id, const QString &shortcut);
    /**
     * @brief saveSound -> method that saves sounds chosen by user to file.
     * @param id -> pad id.
     * @param sound -> pad sound.
     */
    void saveSound(const uint8_t &id, const QString &sound);
    /**
     * @brief loadShortcuts -> method that loads user shortcuts from file.
     * @param index -> pads number.
     * @return -> shortcut name.
     */
    QString loadShortcuts(const uint8_t &index);
    /**
     * @brief loadSounds -> method that loads user sound from file.
     * @param index -> pads number.
     * @return -> sound name.
     */
    QString loadSounds(const uint8_t &index);

private:
    /**
     * @brief m_soundSettings -> pointer to QSettings object responsible for sounds.
     */
    std::unique_ptr<QSettings> m_soundSettings;
    /**
     * @brief m_shortcutSettings -> pointer to QSettings object responsible for shortcuts.
     */
    std::unique_ptr<QSettings> m_shortcutSettings;
};

#endif // USERSETTINGS_H
