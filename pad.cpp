#include "pad.h"

#include <QShortcut>

static const QString M_SAMPLES_PATH = "../drumMachine/SAMPLES/";

Pad::Pad() :
    m_sound(""),
    m_shortcut("")
{
    const uint8_t PAD_WIDTH = 200;
    const uint8_t PAD_HEIGHT= 200;

    setFixedSize(PAD_WIDTH, PAD_HEIGHT);
    setText(m_shortcut);
    setShortcut(m_shortcut);

    player = std::make_unique<QMediaPlayer>();

    connect(this, &Pad::clicked, this, &Pad::playSound);
}

Pad::~Pad()
{

}

void Pad::playSound()
{
    player->stop();
    player->setMedia(QUrl::fromLocalFile(M_SAMPLES_PATH + m_sound));
    player->play();
}

void Pad::setPadSound(const QString &newSound)
{
    m_sound = newSound;
}

QString Pad::getPadShortcut() const
{
    return m_shortcut;
}

QString Pad::getPadSound() const
{
    return m_sound;
}

void Pad::setPadShortcut(const QString &newKey)
{
    m_shortcut = newKey;
    setText(m_shortcut);
    setShortcut(static_cast<QKeySequence>(QString(newKey)));
}
