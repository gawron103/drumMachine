#include "padsconfigurator.h"
#include "ui_padsconfigurator.h"

#include <cmath>

PadsConfigurator::PadsConfigurator(const uint8_t &padsCount, std::vector<std::shared_ptr<Pad>> &pads, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PadsConfigurator),
    m_numberOfPads(padsCount),
    m_pads(pads)
{
    ui->setupUi(this);
}

PadsConfigurator::~PadsConfigurator()
{
    delete ui;
}

void PadsConfigurator::createPadTransformer(const uint8_t &number)
{
    padTransformers.push_back(std::make_shared<PadTransformer>(m_pads[number], this));

    connect(padTransformers.back().get(), PadTransformer::signalShortcutChanged, this, [this](const uint8_t id){
        emit shortcutChanged(id);
    });

    connect(padTransformers.back().get(), PadTransformer::signalSoundChanged, this, [this](const uint8_t id){
        emit soundChanged(id);
    });
}

void PadsConfigurator::placePadTransformers()
{
    const uint8_t TRANSFORMERS_PER_LINE = sqrt(m_numberOfPads);

    uint8_t index = 0;

    for (uint8_t column = 0; column < TRANSFORMERS_PER_LINE; ++column)
    {
        for (uint8_t row = 0; row < TRANSFORMERS_PER_LINE; ++row)
        {
            ui->gridLayout->addWidget(padTransformers[index].get(), column, row);
            index++;
        }
    }
}
