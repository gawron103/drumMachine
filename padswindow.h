#ifndef PADSWINDOW_H
#define PADSWINDOW_H

#include <QMainWindow>

#include <memory>
#include <vector>

#include "startupwidget.h"
#include "pad.h"
#include "padsconfigurator.h"
#include "usersettings.h"

namespace Ui {
class PadsWindow;
}

/**
 * @brief The PadsWindow class -> class responsible for displaying pads and also for
 * writing/reading user settings.
 */
class PadsWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief PadsWindow -> constructor.
     * @param parent -> default parameter of widgets parent.
     */
    explicit PadsWindow(QWidget *parent = nullptr);
    ~PadsWindow();
    /**
     * @brief showStartupWidget -> method that shows startup widget.
     */
    void showStartupWidget();

private:
    Ui::PadsWindow *ui;

    /**
     * @brief m_startupWidget -> pointer of startup widget.
     */
    std::unique_ptr<StartupWidget> m_startupWidget;
    /**
     * @brief m_padsConfigurator -> pointer of pads configurator.
     */
    std::unique_ptr<PadsConfigurator> m_padsConfigurator;

    /**
     * @brief m_padsCounter -> variable that holds number of pads.
     */
    uint8_t m_padsCounter;

    /**
     * @brief createPads -> method that creates as many pads as user wanted.
     * @param numberOfPads -> number of pads that user wants.
     */
    void createPads(const uint8_t &numberOfPads);

    /**
     * @brief m_pads -> vector of pad transformers.
     */
    std::vector<std::shared_ptr<Pad>> m_pads;

    /**
     * @brief saveShortcutSetting -> method that saves shortcuts that user choose for pads.
     * @param id -> number of pad.
     */
    void saveShortcutSetting(const uint8_t &id);
    /**
     * @brief saveSoundSetting -> method that saves sound that user choose for pads.
     * @param id -> number of pad.
     */
    void saveSoundSetting(const uint8_t &id);
    /**
     * @brief loadShortcutSettings -> method that loads previously chosen shortcuts.
     * @param index -> number of pad.
     */
    void loadShortcutSettings(const uint8_t &index);
    /**
     * @brief loadSoundSettings -> method that loads previously chosen sound.
     * @param index -> number of pad.
     */
    void loadSoundSettings(const uint8_t &index);
    /**
     * @brief resetPadsWindow -> method that allows user to choose again pad amount without
     * resetting application.
     */
    void resetPadsWindow();
    /**
     * @brief userSettings -> pointer to user settings.
     */
    std::unique_ptr<UserSettings> userSettings;
};

#endif // PADSWINDOW_H
