#include "padswindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PadsWindow w;
    w.show();
    w.showStartupWidget();

    return a.exec();
}
