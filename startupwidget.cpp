#include "startupwidget.h"
#include "ui_startupwidget.h"

const static uint8_t BUTTONS_AMOUNT_2X2 = 4;
const static uint8_t BUTTONS_AMOUNT_3X3 = 9;
const static uint8_t BUTTONS_AMOUNT_4X4 = 16;

StartupWidget::StartupWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StartupWidget)
{
    ui->setupUi(this);

    ui->rb_2x2->setChecked(true);

    connect(ui->pb_create, &QPushButton::clicked, this, [this](){
        uint8_t amountOfPads = 0;

        if (ui->rb_2x2->isChecked())
        {
            amountOfPads = BUTTONS_AMOUNT_2X2;
        }
        else if (ui->rb_3x3->isChecked())
        {
            amountOfPads = BUTTONS_AMOUNT_3X3;
        }
        else
        {
            amountOfPads = BUTTONS_AMOUNT_4X4;
        }

        emit numberOfPads(amountOfPads);

        close();
    });

    connect(ui->pb_exit, &QPushButton::clicked, this, [this](){
        emit closeApp();
        close();
    });
}

StartupWidget::~StartupWidget()
{
    delete ui;
}
