#ifndef PADTRANSFORMER_H
#define PADTRANSFORMER_H

#include <memory>

#include "pad.h"

namespace Ui {
class PadTransformer;
}

/**
 * @brief The PadTransformer class -> class responsible for creating pad transformer widget, which
 * can change pad sound and shortcut.
 */
class PadTransformer : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief PadTransformer -> constructor.
     * @param pad -> pointer for pad.
     * @param parent -> default parameter of widgets parent.
     */
    explicit PadTransformer(const std::shared_ptr<Pad> &pad, QWidget *parent = 0);
    ~PadTransformer();

private:
    Ui::PadTransformer *ui;

    /**
     * @brief m_id -> variable that holds transformer id.
     */
    int m_id;

    /**
     * @brief setId -> method that sets transformer id. Also it resets id counter if
     * given parameter is equal to true.
     * @param shouldReset -> parameter specifying if id counter should be reset to default.
     * @return -> id
     */
    int setId(const bool shouldReset = false);
    /**
     * @brief chooseNewSound -> method that allows user to choose new sound for pad.
     */
    void chooseNewSound();
    /**
     * @brief createNewShortcut -> method that allws user to chose new sound for pad.
     */
    void createNewShortcut();
    /**
     * @brief changeTransformerLabel -> method that change transformer main label text to
     * actual button shortcut.
     */
    void changeTransformerLabel();
    /**
     * @brief changeSoundLabel -> method that change transformer sound label text to
     * actual sound text.
     */
    void changeSoundLabel();

    /**
     * @brief m_pad -> pointer to pad instance.
     */
    std::shared_ptr<Pad> m_pad;

signals:
    /**
     * @brief signalSoundChanged -> signal for sending which transformer change sound.
     * @param id -> id of the transformer
     */
    void signalSoundChanged(int id);
    /**
     * @brief signalShortcutChanged -> signal for sending which transformer change shortcut.
     * @param id -> id of the transformer.
     */
    void signalShortcutChanged(int id);
};

#endif // PADTRANSFORMER_H
