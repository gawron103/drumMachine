#ifndef PAD_H
#define PAD_H

#include <QPushButton>
#include <QMediaPlayer>

#include <memory>

/**
 * @brief The Pad class -> class responsible for pad logic.
 */
class Pad : public QPushButton
{
public:
    /**
     * @brief Pad -> default constructor.
     */
    Pad();
    ~Pad();

    /**
     * @brief setPadShortcut -> method that sets up given shortcut to the pad
     * @param newKey -> QString of new shortcut.
     */
    void setPadShortcut(const QString &newKey);
    /**
     * @brief setPadSound -> method that sets up given sound
     * @param newSound -> QString of new sound.
     */
    void setPadSound(const QString &newSound);

    /**
     * @brief getPadShortcut -> method that returns current pad sound.
     * Required for saving user settings.
     * @return -> shortcut string.
     */
    QString getPadShortcut() const;
    /**
     * @brief getPadSound -> method that return current pad shortcut.
     * Required for saving user settings.
     * @return -> sound string.
     */
    QString getPadSound() const;

private:
    /**
     * @brief m_sound -> variable which stores current sound.
     */
    QString m_sound;
    /**
     * @brief m_shortcut -> variable which stores current shortcut.
     */
    QString m_shortcut;

    /**
     * @brief playSound -> method that plays current sound when pad is clicked.
     */
    void playSound();
    /**
     * @brief player -> pointer to QMediaPlayer instance.
     */
    std::unique_ptr<QMediaPlayer> player;
};

#endif // PAD_H
