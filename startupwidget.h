#ifndef STARTUPWIDGET_H
#define STARTUPWIDGET_H

#include <QWidget>

namespace Ui {
class StartupWidget;
}

/**
 * @brief The StartupWidget class -> class responsible for choosing amount of pads
 * at the beginning of the program.
 */
class StartupWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief StartupWidget -> constructor.
     * @param parent -> default parameter of widgets parent.
     */
    explicit StartupWidget(QWidget *parent = 0);
    ~StartupWidget();

private:
    Ui::StartupWidget *ui;

signals:
    /**
     * @brief signalNumberOfPads -> signal for sending amount of pads chosen by user.
     * @param numberOfPads -> number of pads.
     */
    void numberOfPads(uint8_t numberOfPads);
    /**
     * @brief signalCloseApp -> signal for closing whole program.
     */
    void closeApp();
};

#endif // STARTUPWIDGET_H
